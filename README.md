Libraries that are portable R7RS Small but use the [PFFI](https://codeberg.org/r7rs-pffi/pffi) library.


# Libraries

## (retropikzel pffi v0-3-0)

Portable foreign function interface for some R7RS Scheme implementations

Versions: v0-2-0 v0-2-2 v0-3-0 


## (retropikzel pffi-shell v0-3-2)

Library to run shell commands

Versions: v0-2-0 v0-2-1 v0-3-0 v0-3-1 v0-3-2 



