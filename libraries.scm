((retropikzel
   (libraries
     (pffi
       (url . "https://codeberg.org/r7rs-pffi/pffi.git")
       (description . "Portable foreign function interface for some R7RS Scheme implementations"))
     (pffi-shell
       (url . "https://codeberg.org/Schubert/pffi-shell.git")
       (description . "Library to run shell commands")))))
